# HW01_Twitter

## DATA LIT COURSE
## Homework Number 1.6: Twitter Sentiment Analysis
#  By: Eduardo Espejel

# NOTES:
Pass the target word as a parameter on the command line.
> Please notice that a separate file with your Twitter API keys is required.

The statistics values should be useful in deciding the word's polarity.
Chief among them, the Mean value's sign and the Standard deviation.

Also, a couple charts are included, both should come handy as visual discriminators:

1. Sentiment Scores Vs Neutrality Line
2. Histogram Vs Mean Value Marker

>
>A pdf sample file is included as an output sample. Enjoy,
>
