#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Spyder Editor
Created on Wed Mar 13 17:22:42 2019
@author: eespejel

20190313
DATA LIT COURSE
1.6 HOMEWORK ASSIGNMENT: TWITTER SENTIMENT ANALYSIS
BY: EDUARDO ESPEJEL

"""

### EXPECT THE WORD TO SEARCH FOR AS A PARAMENTER ON THE COMMAND LINE
import sys
if len(sys.argv) < 2:
    print("-"*80)
    print("-"*80)
    print("TWITTER SENTIMENT ANALYSIS: Please provide the word to search for. Finishing...")
    print("-"*80)
    print("-"*80)
    raise SystemExit
print("-"*80)
print("TWITTER SENTIMENT ANALYSIS: TARGET WORD: =->{}<-=. Running...".format(sys.argv[1]) )
print("-"*80)

# Twitter imports
import tweepy
import twitterKeys as tkeys

# Pandas / Matplot ¡imports
import pandas as pd
import matplotlib.pyplot as plt

# Sentiment Analysis imports
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import nltk
nltk.download('vader_lexicon')
    
if __name__ == "__main__":
    
    # Twitter Access
    auth = tweepy.OAuthHandler( tkeys.consumer_key, tkeys.consumer_secret)
    auth.set_access_token( tkeys.access_token, tkeys.access_token_secret)
    api = tweepy.API(auth)
    
    # Word to search for
    word = sys.argv[1]
    tweets = api.search( word, count=300)
    data = pd.DataFrame( data=[tweet.text for tweet in tweets], columns=['Tweets'])
    
    # Do Sentiment Analysis, and isolate the "COUMPOUND" value for each tweet
    sid = SentimentIntensityAnalyzer()
    listy = []
    for index, row in data.iterrows():
        ss = sid.polarity_scores(row["Tweets"])
        listy.append(ss)
      
    compounds = []
    for c in listy:
        if c["compound"]:   # Let's ignore the neutral opnions...
            compounds.append( c["compound"] )
    
    compounds.sort()

    # Show some Statistics on the sentiments results:
    print("-"*80)
    print( "STATISTICS RESULTS:" )
    print("-"*80)
    AsASseries = pd.Series( compounds )
    print( AsASseries.describe() )

    print("-"*80)
    print( "SUPPORT CHARTS:\n" )
    print("-"*80)
    plt.plot( compounds )
    plt.plot( [0,len(compounds)-1], [0,0] )
    plt.title("COMPOUND SENTIMENT VALUES & 'NEUTRAL' MARKER")
    plt.show()
    
    colVals, _, _ = plt.hist( compounds )
    plt.title("HISTOGRAM & MEAN VALUE MARKER")
    mean = AsASseries.describe()["mean"]
    plt.plot( [ mean, mean], [ 0, colVals.max()] )
    plt.show()
